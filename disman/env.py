"""Close to the simulator, manages the data the agent will use.

The data come from the simulator, are arranged for the agent by the
environment and then sent to the agent
"""
from functools import reduce
from math import factorial
from typing import Iterable, Dict, Mapping, Optional, NamedTuple, Set, Tuple

from numpy import log2
import numpy as np

from disman import DEFAULT_SIMPARAM
from disman import simulator as Simulator  # noqa: N812

Info = Dict
"""Type of the returned additional informations."""
_ADDINFO = set(["reward"])
"""The information added by this module."""
INFOKEYS = Simulator.INFOKEYS | _ADDINFO
"""Each entry contains the entries defined in Simulator plus a reward."""

Action = int
"""Number of aircraft to be swapped with."""

State = NamedTuple(
    "State",
    [
        ("schedule", Simulator.Schedule),
        ("fleet", Tuple[str, ...]),
        ("landed", str),
        ("delays", Mapping[str, int]),
        ("assignment", Mapping[str, int]),
        ("remaining", Mapping[str, int]),
        ("d_rd_sobt", Mapping[str, Optional[int]]),
    ])
"""The assignment maps each aircraft to a flight path and a delay
indicator.

Args:
    schedule: contains data for the underlying model
    fleet: list of tail numbers, immutable to keep an order
           among aircraft
    landed: tail number of lastly landed aircraft
    delays: mapping from aircraft to delays
    assignment: mapping from aircraft to flight path; flight path are
                identified by an integer, which is the index of the aircraft
                they are initially assigned to.
    remaining: number of remaining flights of each aircraft; warning
               highly time dependent
    d_rd_sobt: time delta between ready time of the landed aircraft and sobt of
               other aircraft
    """


_TIME_CARD = 2
"""Number of time values"""


def reset_delays(state: State, delays: int) -> State:
    """Reset delayed flights."""
    newsched = Simulator.set_delays(state.schedule, delays)
    return State(
        schedule=newsched,
        fleet=state.fleet,
        landed=state.landed,
        delays=state.delays,
        assignment=state.assignment,
        remaining=state.remaining,
        d_rd_sobt=state.d_rd_sobt)


def load(schedfile: str, simparam: Mapping = DEFAULT_SIMPARAM) -> State:
    """Loads a schedule file."""
    schedule = Simulator.load(schedfile, simparam)
    landed = Simulator.aircraft(schedule)

    tail_numbers = tuple(sorted(Simulator.fleet(schedule)))

    remaining = {a: Simulator.remaining_flights(schedule, a)
                 for a in tail_numbers}
    sobt_diff = {a: None for a in tail_numbers}

    lst = State(schedule=schedule,
                landed=landed,
                fleet=tail_numbers,
                assignment={s: i for i, s in enumerate(tail_numbers)},
                delays={s: 0 for s in tail_numbers},
                remaining=remaining,
                d_rd_sobt=sobt_diff)
    return lst


def idle(state: State) -> Action:
    """Returns the idle action (don't do anything), i.e. the index
    of the landed aircraft."""
    return state.fleet.index(Simulator.aircraft(state.schedule))


def step(state: State, action: Action) -> Tuple[State, float, bool, Dict]:
    """Applies an action on a state.

    The action a is applied on the state s.  A reward is obtained from
    this action.  If the new state is terminal, then the 3rd element
    is True.

    Args:
        s: state of the environment
        a: action to be applied on the environment

    Returns:
        (ns, r, d, i) where `ns` is the new state resulting from the
        application of a on `s`, `r` is the reward associated to the
        action, `d` indicates whether the new state is terminal and `i`
        contains miscellaneous information.
    """
    # Apply the action on the environment
    simaction = state.fleet[action]
    newschedule, cost, done, info = Simulator.step(state.schedule, simaction)

    landed = Simulator.aircraft(state.schedule)
    landed_fp = state.assignment[landed]
    swapped_acft = next(acft for acft in state.assignment
                        if state.assignment[acft] == action)
    nassg = {**state.assignment, **{landed: action,
                                    swapped_acft: landed_fp}}
    delays = {
        acft:
        Simulator.current_delay(newschedule, acft)
        for acft in Simulator.fleet(newschedule)}
    remaining = {a: Simulator.remaining_flights(newschedule, a)
                 for a in state.fleet}
    drdsobts = {a: Simulator.d_ready_sobt(newschedule, a)
                for a in newschedule.fleet}

    newstate = State(schedule=newschedule, fleet=state.fleet,
                     landed=Simulator.aircraft(newschedule),
                     assignment=nassg, delays=delays,
                     remaining=remaining, d_rd_sobt=drdsobts)

    reward = -cost

    newinfo = {**info, **{"reward": reward}}

    return newstate, reward, done, newinfo


def get_action_set(state: State) -> Set[Action]:
    """Returns all the aircraft index that can be swapped with the landed."""
    selfleet = filter(lambda a: Simulator.swappable(state.schedule, a),
                      state.fleet)
    return (set(state.fleet.index(a) for a in selfleet) |
            set([state.fleet.index(state.landed)]))


def _c2b(card: int) -> int:
    """Returns the number of bits required to encode a datum which might have c
    different values."""
    return int(log2(card)) + 1


def _int_of_bool_vec(bvec: Iterable[bool]) -> int:
    return reduce(lambda acc, elt: acc | (elt[1] << elt[0]),
                  enumerate(bvec), 0)


def _catint(int_a: int, int_b: int, size_b: int) -> int:
    """Binary wise concatenation.

    Args:
        int_a: int added on top
        int_b: int at the bottom of the stack
        size_b: number of bits of int_b

    Returns: something like int_a + int_b, where `+` would be
    an infix concatenation operator
    """
    return int_b | (int_a << size_b)


def _aircraft_spec(state: State, aircraft: str) -> Tuple[bool, bool, bool]:
    """Give aircraft spec vector of a state

    Args:
        aircraft: tail number of the aircraft
    Returns: vector containing
        * 1 if sobt of next swappable flight is before the ready time
          of the landed aircraft, 0 otherwise,
        * 1 if aircraft has more remaining flights than landed one, 0
          otherwise,
        * 1 if aircraft is directly going to land at the station, 0
          otherwise
        If aircraft is not swappable, return 0
    """
    if not Simulator.swappable(state.schedule, aircraft):
        return 0, 0, 0
    sobt_before = (state.d_rd_sobt[aircraft] > 0
                   if state.d_rd_sobt[aircraft] else 0)
    more_rem = state.remaining[aircraft] > state.remaining[state.landed]
    sw_isnext = Simulator.flying_toward(state.schedule, aircraft)
    return sobt_before, more_rem, sw_isnext


def int_of_state(state: State) -> int:
    """Convert state to int."""
    fleetcard = len(Simulator.fleet(state.schedule))
    aircraft_reqbits = 3
    acft_specs = tuple(_aircraft_spec(state, a) for a in state.fleet)
    acft_asint = tuple(_int_of_bool_vec(abv) for abv in acft_specs)
    ifleet = reduce(lambda acc, elt: _catint(elt, acc, aircraft_reqbits),
                    acft_asint, 0)
    ndelays = sum(Simulator.current_delay(state.schedule, a)
                  for a in state.fleet)
    laid = state.fleet.index(state.landed)
    ifleet_del = _catint(ifleet, ndelays, _c2b(fleetcard))
    return _catint(ifleet_del, laid, _c2b(fleetcard))


def array_of_state(state: State) -> np.ndarray:
    """Convert a state to an array, for neural networks."""
    rem_ratio = [(np.inf if state.remaining[a] == 0
                  else state.remaining[state.landed] / state.remaining[a])
                 for a in state.fleet]
    outarr = np.concatenate(([state.delays[a] for a in state.fleet],
                             [state.assignment[a] for a in state.fleet],
                             rem_ratio,
                             [state.fleet.index[state.landed]]))
    return outarr


def action_of_int(act_i: int) -> Action:
    """Bijective conversion from int to action."""
    return act_i


def int_of_action(action: Action) -> int:
    """Bijective conversin from action to int."""
    return action


def state_card(state: State) -> int:
    """Returns the number of possible states"""
    fleet_size = len(Simulator.fleet(state.schedule))
    return fleet_size * int(2 ** fleet_size) * factorial(fleet_size)


def state_arr_len(state: State) -> int:
    """Give the length of an array representing a state."""
    nfleet = len(state.fleet)
    # + 1 for the landed aircraft
    return 1 + 3 * nfleet


def state_max_repr(state: State) -> int:
    """Return sup{int_of_state(x) | x in S}."""
    fleetcard = len(Simulator.fleet(state.schedule))
    aircraft_reqbits = (2 + _c2b(_TIME_CARD) +
                        _c2b(fleetcard))
    fleet_reqbits = fleetcard * aircraft_reqbits
    laid_reqbits = _c2b(fleetcard)
    return 1 << (fleet_reqbits + laid_reqbits + 1)


def action_card(state: State) -> int:
    """Returns the number of possible actions."""
    return len(state.assignment)


def action_max_repr(state: State) -> int:
    """Returns sup{int_of_action(x) | x in A}."""
    return action_card(state)
