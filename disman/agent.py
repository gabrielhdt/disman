"""Q learning agent using a matrix."""
from typing import Any, Dict, Hashable, List, NamedTuple, Set, Tuple

import numpy as np
from scipy import sparse

import disman.env as Env

Info = Dict
"""Type of information returned."""
_ADDINFO = set(["flight", "cum_reward", "visitcount", "action", "q"])
"""Information added in this module."""
INFOKEYS = Env.INFOKEYS | _ADDINFO
"""Keys contained in information dict."""

T = NamedTuple("T", [("matrix", sparse.spmatrix),
                     ("default", float)])
"""Q matrix and default value."""
Action = int
State = int

VisitcountData = Dict[Tuple[Hashable, Hashable], int]
"""Counts state/action visits, for learning rate"""


def load(initstate: Env.State, fpath: str) -> T:
    """Returns an agent."""
    mat = sparse.load_npz(fpath).todok()
    default = idle(initstate)[0]
    return T(matrix=mat, default=default)


def save(ag: T, fpath: str) -> None:
    """Saves an agent matrix to disk."""
    sparse.save_npz(fpath, ag.matrix.tocsc())


def _quality(ag: T, s: State, a: Action) -> float:
    """Q value."""
    return ag.matrix.get((s, a), default=ag.default)


def _set_quality(ag: T, s: State, a: Action, newq: float) -> T:
    """Updates agent."""
    newmat = ag.matrix.copy()
    newmat[s, a] = newq
    return T(matrix=newmat, default=ag.default)


def _best_reward(agent: T, state: State) -> np.ndarray:
    col = agent.matrix.shape[1]
    return np.amax([_quality(agent, state, a) for a in range(col)])


def _greedy_policy(agent: T, state: State, action_set: Set[Action],
                   eps: float) -> Action:
    """Chooses an action among an action set following a greedy policy."""
    if np.random.random() <= eps:
        sel_act = np.random.choice(list(action_set))
    else:
        act_qualities = set((a, _quality(agent, state, a)) for a in action_set)
        sel_act = max(act_qualities, key=lambda x: x[1])[0]
    return sel_act


def _ucb_policy(agent: T, state: State, action_set: Set[Action],
                visitcount: VisitcountData, step: int, exp: float) -> Action:
    """Chooses an action using upper confidence bound bandit
    method."""
    def ucb(quality: float, step: int, visits: int, ctrl: float) -> float:
        assert step >= 0 and visits >= 0
        return quality + ctrl * np.sqrt(np.log(1 + step) / (1 + visits))
    act_qualities = set((a, _quality(agent, state, a)) for a in action_set)
    act_ucb = set((aq[0],
                   ucb(aq[1], step, visitcount.get((state, aq[0]), 0), exp))
                  for aq in act_qualities)
    return max(act_ucb, key=lambda au: au[1])[0]


def pick_states(state: State, amount: int = None) -> Set[State]:
    """Pick some states randomly.

    Used to assess evolution of the agent.
    Args:
        init: the initial state
        amount: number of states to pick

    Returns: a set of states chosen randomly
    """
    states = set()
    istate = Env.int_of_state(state)
    done = False
    while not done:
        states.add(istate)
        act_set = Env.get_action_set(state)
        action = np.random.choice(list(act_set))
        state, _, done, _ = Env.step(state, action)
        istate = Env.int_of_state(state)
    return (states if amount is None
            else set(np.random.choice(list(states)) for _ in range(amount)))


def eval_states(agent: T, states: Set[State]) -> float:
    """Return the average of the max Q value of each state.

    For each state in the set, get the max Q value along actions,
    and returns the average.  This is the metric used in Google's
    DeepQ learning article."""
    highest_qs = set(_best_reward(agent, s) for s in states)
    return sum(highest_qs) / len(highest_qs)


def train(ag: T, init: State,
          discount: float,
          eve: float,
          visc: VisitcountData,
          step: int) -> Tuple[T, float, Tuple[VisitcountData, int],
                              List[Dict]]:
    """Train the agent over one session.

    Args:
        ag: matrix of the agent
        init: the initial state
        discount: update parameter
        eve: exploration v. exploitation trade off parameter

    Returns:
        a tuple containing

        * the trained agent,
        * the cumulative reward,
        * a tuple containing

          * the visitcount dictionary
          * the number of simulations carried out in total

        * a list of information dictionaries
    """
    data = []  # type: List[Dict]

    state = init
    istate = Env.int_of_state(state)
    done = False
    accrew = 0
    f = 0  # To save the flight number
    while not done:
        # Choose next action
        act_set = Env.get_action_set(state)
        iact_set = set(Env.int_of_action(a) for a in act_set)
        iaction = _ucb_policy(ag, istate, iact_set, visc, step + f, eve)
        action = Env.action_of_int(iaction)

        # Perform action
        nstate, reward, done, info = Env.step(state, action)
        instate = Env.int_of_state(nstate)

        # Update visits count
        try:
            visc[(istate, iaction)] += 1
        except KeyError:
            visc[(istate, iaction)] = 1
        learning_rate = 1 / visc[(istate, iaction)]

        # Update agent
        upd_val = (
            _quality(ag, istate, iaction) +
            learning_rate *
            (reward + discount * _best_reward(ag, instate) -
             _quality(ag, istate, iaction)))
        ag.matrix[istate, iaction] = upd_val

        accrew += reward
        data.append({**info, **{"flight": f,
                                "cum_reward": accrew,
                                "visitcount": visc[(istate, iaction)],
                                "q": _quality(ag, istate, iaction),
                                "action": iaction}})

        # Prepare next loop
        f += 1
        state = nstate
        istate = Env.int_of_state(state)
    return ag, accrew, (visc, step + f), data


def idle(init) -> Tuple[float, Dict]:
    """Idle agent, does not do anything"""
    observation = init
    done = False
    data = []  # type: List[Dict]
    accrew = 0.
    vc = {}
    f = 0
    while not done:
        action = Env.idle(observation)
        iaction = Env.int_of_action(action)
        observation, reward, done, info = Env.step(observation, iaction)
        # Always choose action 0
        iobs = Env.int_of_state(observation)
        try:
            vc[(iobs, 0)] += 1
        except KeyError:
            vc[(iobs, 0)] = 1
        accrew += reward
        data += [{**info, **{"flight": f,
                             "cum_reward": accrew,
                             "q": None,
                             "visitcount": vc[(iobs, 0)],
                             "action": 0}}]
        f += 1
    return accrew, data


def rollout(ag: T, init) -> Tuple[float, Dict]:
    """Plays a session using an agent."""
    observation = init
    iobs = Env.int_of_state(observation)
    done = False
    accrew = 0.
    data = []  # type: List[Dict]
    vc = {}  # type: Dict[Tuple[Any, Any], int]
    f = 0
    while not done:
        act_set = Env.get_action_set(observation)
        iact_set = [Env.int_of_action(a) for a in act_set]
        iaction = _greedy_policy(ag, iobs, iact_set, eps=0.0)
        action = Env.action_of_int(iaction)

        observation, reward, done, info = Env.step(observation, action)
        iobs = Env.int_of_state(observation)

        try:
            vc[(iobs, action)] += 1
        except KeyError:
            vc[(iobs, action)] = 1

        accrew += reward
        data += [{**info, **{"flight": f,
                             "q": None,
                             "cum_reward": accrew,
                             "visitcount": vc[(iobs, action)],
                             "action": iaction}}]
        f += 1
    return accrew, data


def random(init) -> Tuple[float, Dict]:
    """Selects actions randomly."""
    observation = init
    done = False
    accrew = 0.
    data = []  # type: List[Dict]
    f = 0
    while not done:
        act_set = Env.get_action_set(observation)
        action = np.random.choice(list(act_set))
        observation, reward, done, info = Env.step(observation, action)
        accrew += reward
        data += [{**info, **{"flight": f,
                             "q": None,
                             "cum_reward": accrew,
                             "visitcount": None,
                             "action": Env.int_of_action(action)}}]
        f += 1
    return accrew, data


def make(init: Env.State, default: float = 0.) -> T:
    """Creates a sparse matrix, rows for states and columns for actions."""
    nstates = Env.state_max_repr(init)
    nactions = Env.action_max_repr(init)
    mat = sparse.dok_matrix((nstates, nactions), dtype=np.float64)
    return T(matrix=mat, default=default)
