"""Defines types of environment"""

from typing import Dict, Tuple, Mapping, NamedTuple, Optional
from disman import DEFAULT_SIMPARAM
from disman import simulator as Simulator  # noqa: N812

State = NamedTuple(
    "State",
    [
        ("schedule", Simulator.Schedule),
        ("fleet", Tuple[str, ...]),
        ("landed", str),
        ("delays", Mapping[str, int]),
        ("assignment", Mapping[str, int]),
        ("remaining", Mapping[str, int]),
        ("d_rd_sobt", Mapping[str, Optional[int]]),
    ])
Action = int


def step(s: State, a: Action) -> Tuple[State, float, bool, Dict]: ...


def load(schedfile: str, simparam: Mapping = DEFAULT_SIMPARAM) -> State: ...


def int_from_state(s: State) -> int: ...


def int_from_action(a: Action) -> int: ...
