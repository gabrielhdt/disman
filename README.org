* Disruption manager

  Manage schedule disruption via an agent trained by Q learning

** Training the manager
   To train the agent,
   #+begin_src shell
     ./tactical_optimiser.py --schedule <schedfile> --prob_rate e
     --discount g --iterate n --severe_delay_prob s
   #+end_src
   to launch =n= trainings with initla random choice frequency =e=,
   discount parameter =g=, using schedule specified in =schedfile=,
   with a probability of severe delay per flight of =s=.
   

** Data files
*** Schedule
    The schedule is in a =csv= format.  It contains all flights, with
    performed during the day.  The table must have the following
    columns
      + =ac_type=, the model of the aircraft;
      + =tail_number=, aircraft identifier;
      + =origin_airport= from where the flight departs;
      + =destination_airport= where does the flight go;
      + =scheduled_departure= the scheduled off block time, the time,
        without atfm delay, when the aircraft should depart;
      + =actual_fp_time= the duration of the flight, from take off to
        touchdown.
	
    The schedule can be chosen on command line via the argument
    =--schedule=.
	
*** Delays
    Data delays are entered statically (cannot be chosen via command
    line) in =json= files in the =data/= folder.  As the data may not
    be complete enough to cover the schedules, some entries have been
    added manually by duplicating other ones.  Those wrong entries are
    at the end of the =json= files, separated by a newline.
    
** Output
*** Agent output
    If used with option =--save_q <qpath>=, the agent will be saved to
    the file of path =<qpath>=.  The agent can be loaded back with
    option =--load_q <qpath>=.  An agent can be loaded either to
    continue its training or to test it with option =--rollout=.
    
*** Simulator output
    If invoked with the option =--save_res <out>=, the simulator
    will output files in the /parquet/ file format.  These can be read
    by =pandas= with
    #+begin_src python
      import pandas as pd
      data = pd.read_parquet(fpath)
    #+end_src
    Their main use is for debugging.
    
*** Compare
    The =compare.py= script is used to compare an agent with the idle
    behaviour.  Call
    #+begin_src shell
      ./compare.py --schedule s --agent a --iterate n [--out o]
      [--delays d] [--severe_prob sp]
    #+end_src
    to compare the agent stored in file =a= performing =n= iterations
    of schedule =s= with =d= delays per episode and an additive
    probability of severe delay =sp=.  Save the cumulative rewards of
    the episodes into =o=.  The full reports of the runs are saved
    into =<o>_idle.parquet= and =<o>_agent.parquet= where =<o>= stands
    for the argument of =--out=.
    
    The delays introduced via the =--delays= argument are identical
    for the agent and the idle, making the comparison accurate.  On
    the opposite, delays introduced via the probability may differ
    heavily between the idle run and the agent run.

** Doc
   The documentation is in the =doc/= directory and can be built via a
   makefile,
   #+begin_src sh
     make html
   #+end_src
   will create the =html= doc.  The files are created by emacs.
