"""Processes info dicts"""
from typing import Dict, List

import pandas as pd

from disman import agent as Agent


def _make_sequence_from_episode(episode: List[Dict], key: str) -> pd.Series:
    """Create a pd series according to a key of the data of an episode."""
    return [d[key] for d in episode]


def _episode_to_dataframe(episode: List[Dict], ep_no: int) -> pd.DataFrame:
    """Converts an episode to a dataframe."""
    series = {k: _make_sequence_from_episode(episode, k)
              for k in Agent.INFOKEYS}  # type: ignore
    df = pd.DataFrame(series)
    df['episode'] = ep_no
    return df


def process(episodes: List[List[Dict]]) -> pd.DataFrame:
    """Formats info dictionaries and rewards."""
    dfs = [_episode_to_dataframe(ep, no) for no, ep in enumerate(episodes)]
    # Indexes are ignored to allow more operations
    form = pd.concat(dfs, ignore_index=True)
    return form


def save(fname: str, processed) -> None:
    """Saves processed data to disk."""
    processed.to_parquet(fname + ".parquet")
